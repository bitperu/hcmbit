package pe.com.bit.hcm.control;

import pe.com.bit.hcm.model.Usuario;

import javax.ejb.Local;

/**
 * Created by whuaringa on 17/02/2017.
 */
@Local
public interface UsuarioController {
    Usuario login(String usuario,String clave);


}
