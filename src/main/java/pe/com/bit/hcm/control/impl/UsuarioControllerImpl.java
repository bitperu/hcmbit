package pe.com.bit.hcm.control.impl;

import pe.com.bit.hcm.control.UsuarioController;
import pe.com.bit.hcm.dao.UsuarioDAO;
import pe.com.bit.hcm.model.Usuario;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by whuaringa on 17/02/2017.
 */
@Stateless
public class UsuarioControllerImpl implements UsuarioController {

    @Inject
    UsuarioDAO dao;

    public Usuario login(String usuario, String clave) {
        return dao.getUser(usuario,clave);
    }
}
