package pe.com.bit.hcm.service;

import pe.com.bit.hcm.control.UsuarioController;
import pe.com.bit.hcm.model.Usuario;

import javax.annotation.ManagedBean;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by whuaringa on 17/02/2017.
 */
@ManagedBean
@RequestScoped
@Named
public class UsuarioService {
    @Inject
    UsuarioController controller;
    private static final String SUCCESS="menu";

    private Usuario usuario;

    @PostConstruct
    private void init(){
        usuario=new Usuario();
    }

    public String login(){
        if(
        controller.login(usuario.getUsuario(),usuario.getClave())!=null){
            return SUCCESS;
        }else{
            return "Nada";
        }
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
}
