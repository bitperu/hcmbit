package pe.com.bit.hcm.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by whuaringa on 17/02/2017.
 */
@Entity
@Table(name = "USUARIO")
@NamedQuery(name="Usuario.getUser",query = "SELECT u FROM Usuario  u where u.usuario=:pusuario AND u.clave=:pclave")
public class Usuario implements Serializable{
    @Id
    @Basic(optional = false)
    @Column(name = "USUARIO")
    private String usuario;
    @Basic(optional = false)
    @Column(name = "CLAVE")
    private String clave;

    public Usuario() {
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }
}
