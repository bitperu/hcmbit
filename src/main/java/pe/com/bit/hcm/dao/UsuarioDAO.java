package pe.com.bit.hcm.dao;

import pe.com.bit.hcm.model.Usuario;

import javax.ejb.Local;

/**
 * Created by whuaringa on 17/02/2017.
 */
@Local
public interface UsuarioDAO {
    Usuario getUser(String usuario,String clave);
}
