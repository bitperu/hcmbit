package pe.com.bit.hcm.dao.impl;

import pe.com.bit.hcm.dao.UsuarioDAO;
import pe.com.bit.hcm.model.Usuario;

import javax.ejb.Stateless;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created by whuaringa on 17/02/2017.
 */
@Stateless
public class UsuarioDAOImpl implements UsuarioDAO {
    @PersistenceContext(unitName = "perUnithcmBIT")
    EntityManager entityManager;
    public Usuario getUser(String usuario,String clave){
        return  (Usuario)entityManager.createNamedQuery("Usuario.getUser")
        .setParameter("pusuario",usuario)
        .setParameter("pclave",clave).getResultList().get(0);
    }
}
